import {ADD_LIST, DELETE_LIST, GET_LISTS} from '../actions/ListAction';

const initialState = {
    dataOfLists: null,
};
export default function(state = initialState, action) {
    switch(action.type) {
        case GET_LISTS:
            return { ...state,
                dataOfLists: action.lists
            };
        case ADD_LIST:

            if (action.payload){

                let newList = [...state.dataOfLists, action.payload];
                console.log(' ADD_LIST newList !!====> ', newList );
                return {
                    dataOfLists: newList
                };
            }else{
                return { ...state,
                    dataOfLists: action.lists
                };
            }
        case DELETE_LIST:
            if (action.payload){
                let newLists = action.lists.dataOfLists.filter((el) => {
                    return el.id !== action.payload
                });
                return {
                    dataOfLists: newLists
                };
            }else{
                return { ...state,
                    dataOfLists: action.lists
                };
            }

        default:
            return state;
    }
}