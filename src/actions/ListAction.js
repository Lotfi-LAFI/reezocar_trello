import lists_json from '../data/Lists.json';
export const ADD_LIST = "ADD_LIST";
export const GET_LISTS = "GET_LISTS";
export const DELETE_LIST = "DELETE_LIST";


export function getLists() {
    return {
        type: ADD_LIST,
        lists: lists_json
    }
}
export function addList(list) {
    return {
        type: ADD_LIST,
        payload: list
    }
}

export function deleteList(id,lists) {
    return {
        type: DELETE_LIST,
        payload: id,
        lists
    }
}




