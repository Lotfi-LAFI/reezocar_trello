import React, { useState } from 'react';
import { StyleSheet, css } from 'aphrodite';
import close from '../../icons/close.png';
import {deleteList, getLists} from '../../actions/ListAction';
import {connect} from "react-redux";
import Cards from '../cards/Cards';

class List extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }
    handelDelete = ()=> {

        this.props.deleteList(this.props.id, this.props.lists);
    };
    render(){
        const {id, title, cards, lists} = this.props;
        console.log('List num = ',id ,title);
        return(
            <div className={css(styles.listWrapper)}>

                <div className={`row ${css(styles.fontBody)}`}>
                    <div className={`col-sm-10 col-md-10 col-lg-10 col-xl-10 ${css(styles.titleListStyle)}`}><span>{title}</span></div>
                    <div className={`col-sm-2 col-md-2 col-lg-2 col-xl-2 ${css(styles.closeListStyle)}`}>
                        <img src={close} className={css(styles.closeStyle)} onClick={() => this.handelDelete()} />
                    </div>
                    <br />
                    <Cards listOfCards={cards} />
                </div>


            </div>
        );
    }
}

const  mapStateToProps = (state) => ({
    lists: state.lists
});

export default connect(mapStateToProps, {getLists, deleteList})(List);

const styles = StyleSheet.create({
    listWrapper: {
        width: '272px',
        height: '78px',

    },
    fontBody: {
        width: '272px',
        height: '68px',
        color: '#172b4d',
        fontFamily: '-apple-system,BlinkMacSystemFont,Segoe UI,Roboto,Noto Sans,Ubuntu,Droid Sans,Helvetica Neue,sans-serif',
        fontSize: '14px',
        lineHeight: '20px',
        fontWeight: '400',
        backgroundColor: '#ebecf0',
        borderRadius: '3px',

    },
    closeStyle: {
        height: '10px',
        width: '10px',
        cursor: 'pointer',
    },
    titleListStyle: {
      backgroundColor: '#ebecf0',
        marginTop: '4px',
        fontWeight: '600',
        padding: '4px 8px',
    },
    closeListStyle: {
        marginTop: '4px',
        padding: '4px 22px',
        fontWeight: '600',

    },
});