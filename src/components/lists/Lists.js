import React, { useState } from 'react';
import {connect} from 'react-redux'
import {ListGroup} from 'react-bootstrap';
import { StyleSheet, css } from 'aphrodite';
import {getLists} from '../../actions/ListAction';
import List from './List';
import AddList from './AddList';

class Lists extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            titreValue: '',
        };
    }
    componentDidMount() {
        this.props.getLists();
    }


    render(){
        const {lists} = this.props || [];
        return(
            <div className={css(styles.boardCanvas)}>
                <ListGroup horizontal className={css(styles.allLists)}>
                    {lists.dataOfLists ? lists.dataOfLists.map(element => (
                        <ListGroup.Item key={element.id} className={css(styles.listWrapper)}>
                            <List id={element.id} title={element.title} cards={element.cards} />
                        </ListGroup.Item>
                        ))
                        : null}
                    <ListGroup.Item className={css(styles.addListStyle)}>
                        <AddList />
                    </ListGroup.Item>
                </ListGroup>
            </div>
        );
    }
}

const  mapStateToProps = (state) => ({
    lists: state.lists
});

export default connect(mapStateToProps, {getLists})(Lists);

const styles = StyleSheet.create({
    addListStyle: {
        border: 'none',
        marginLeft: '-5px !important',
        paddingLeft: '0px',
        paddingTop: '10px',
       backgroundColor: '#4bbf6b'
    },
    listWrapper: {
        padding: '10px 0px',
        marginLeft: '10px',
        border: 'none',
        backgroundColor: '#4bbf6b',

    },
    allLists: {
        marginLeft: '15px',
    }
});
