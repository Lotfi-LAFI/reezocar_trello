import React, { useState } from 'react';
import { StyleSheet, css } from 'aphrodite';

class Card extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }

    render(){
        const {id, title} = this.props;
        console.log('card  = ',this.props);
        return(
            <div className={`row ${css(styles.fontBody)}`}>
                 <div className="col-sm-10 col-md-10 col-lg-10 col-xl-10 ">
                        <span>{title}</span>
                 </div>
                <div className="col-sm-2 col-md-2 col-lg-2 col-xl-2">
                </div>

            </div>
        );
    }
}



export default Card;

const styles = StyleSheet.create({
    listWrapper: {
        width: '256px',
        height: '32px',
        backgroundColor: 'yellow'

    },
    fontBody: {
        width: '256px',
        height: '32px',
        backgroundColor: '#fff',
        color: '#172b4d',
        fontFamily: '-apple-system,BlinkMacSystemFont,Segoe UI,Roboto,Noto Sans,Ubuntu,Droid Sans,Helvetica Neue,sans-serif',
        fontSize: '14px',
        lineHeight: '20px',
        fontWeight: '400',
        borderRadius: '3px',

    },
    closeStyle: {
        height: '10px',
        width: '10px',
        cursor: 'pointer',
    },
    titleListStyle: {
        backgroundColor: '#ebecf0',
        marginTop: '4px',
    },
    closeListStyle: {
        marginTop: '4px',
    },
});