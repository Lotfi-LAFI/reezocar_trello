import React, { useState } from 'react';
import {connect} from 'react-redux'
import {Button, Collapse, Form} from 'react-bootstrap';
import { StyleSheet, css } from 'aphrodite';
import {addList, getLists} from '../../actions/ListAction';

import close from '../../icons/close.png';


class AddCard extends React.Component {
    constructor(props) {
        super(props);
        this.state = {

        };

    }


    handleChange = (event) => {
        this.setState({titreValue: event.target.value});
    };
    keyAddList = (event) => {
        if (event.key === 'Enter') {
            this.handelAddList();
        }
    };
    handelClose = () => {
        this.setState({
            titreValue: '',
            open: false
        });
    };

    handleOnBlur = () => {
        console.log('on blur !!');
        this.handelClose();
    };

    handleClick = () => {
        this.setState({
            titreValue: '',
            open: !this.state.open
        });
    };

    render(){
        return(

            <div className={css(styles.containerAccordion)}>
                <div aria-controls="example-collapse-text"
                     aria-expanded={this.state.open}
                     className={css(styles.containerAccordionToggle, this.state.open ? styles.hiddenHead : styles.showHead)}
                     onClick={this.handleClick}
                >

                            <span>Ajouter une liste </span>

                </div>
                <Collapse in={this.state.open}>
                    <div id="example-collapse-text">
                        <div className={css(styles.containerAccordionCollapseCardBody)}>
                            <div className={css(styles.containerForm)}>
                                <Form.Group controlId="exampleForm.ControlInput1">
                                    <Form.Control type="text" value={this.state.titreValue}
                                                  ref={c => (this._input = c)}
                                                  placeholder="Saisissez le titre de la liste"
                                                  onKeyDown={(e) => this.keyAddList(e) }
                                                  onChange={(event) => this.handleChange(event)}
                                                  onBlur={() => this.handleOnBlur() }
                                    />
                                </Form.Group>

                                <div className={`row ${css(styles.rowStyle)}`}>
                                    <div  className={`col-sm-6 col-lg-6 ${css(styles.containerActionStyle)}`}>
                                        <Button className={css(styles.submitStyle, styles.fontBody)} onClick={() => this.handelAddList()}>
                                            Ajouter une liste
                                        </Button>
                                    </div>
                                    <div  className={`col-sm-1 col-lg-1 ${css(styles.containerActionCloseStyle)}`}>
                                        <img src={close} className={css(styles.closeStyle)} onClick={() => this.handelClose()}/>
                                    </div>
                                    <div  className={`col-sm-5 col-lg-5 ${css(styles.containerActionStyle)}`}>

                                    </div>

                                </div>

                            </div>
                        </div>
                    </div>
                </Collapse>
            </div>

        );
    }
}



export default AddCard;

const styles = StyleSheet.create({
    containerAccordion: {
        cursor: 'pointer',
       // borderRadius: '3px',
        minHeight: '28px',
        width: '228px',
        height: '28px',
    },
    hiddenHead: {
        display: 'none'
    },
    showHead: {
        display: 'block'
    },
    containerAccordionToggle: {
        backgroundColor: '#76ce8e',
        cursor: 'pointer',
        borderRadius: '3px',
        width: '272px',
        height: '40px',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        color: '#fff',
    },
    containerAccordionCollapseCardBody: {
        backgroundColor: '#ebecf0',
        padding: '4px',
    },
    containerForm: {
        //backgroundColor: 'red',
        //margin: '0px'
    },
    containerActionStyle: {
        paddingRight: '0px',
    },
    containerActionCloseStyle: {
        paddingRight:  '0px',
        paddingLeft: '0px',
        color: '#172b4d',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
    },
    rowStyle: {
        marginTop: '-10px',
    },
    faTimesStyle: {
        color: '#42526e',
        height: '32px',
        lineHeight: '32px',
        width: '32px',
    },
    actionStyle: {
        backgroundColor: 'red',
        height: '32px'
    },
    submitStyle: {
        float: 'left',
        minHeight: '32px',
        height: '32px',
        backgroundColor: '#5aac44',
        boxShadow: 'none',
        border: 'none',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
    },
    accordion: {
        backgroundColor: '#eee',
        color: '#444',
        cursor: 'pointer',
        padding: '18px',
        width: '100%',
        textAlign: 'left',
        border: 'none',
        outline: 'none',
        transition: '0.4s',
    },
    'active, accordion:hover': {
        backgroundColor: '#ccc',
    },

    fontBody: {
        color: '#fff',
        fontFamily: '-apple-system,BlinkMacSystemFont,Segoe UI,Roboto,Noto Sans,Ubuntu,Droid Sans,Helvetica Neue,sans-serif',
        fontSize: '14px',
        lineHeight: '20px',
        fontWeight: '400',
    },

    closeStyle: {
        height: '15px',
        width: '15px',
    }


});