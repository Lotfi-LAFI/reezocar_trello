import React, { useState } from 'react';
import {ListGroup, Accordion, Form} from 'react-bootstrap';
import { StyleSheet, css } from 'aphrodite';
import Card from './Card';

class Cards extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }

    render(){
        const { listOfCards} = this.props || [];
        return(
            <div className={css(styles.boardCanvas)}>
                <ListGroup>
                    {listOfCards ? listOfCards.map(element => (
                            <ListGroup.Item key={element.id} className={css(styles.listWrapper)}>
                                <Card id={element.id} title={element.title} />
                            </ListGroup.Item>
                        ))
                        : null}

                </ListGroup>
            </div>
        );
    }
}



export default Cards;

const styles = StyleSheet.create({

    listWrapper: {
        paddingLeft: '20px',
        paddingTop: '4px',
        paddingBottom: '4px',
        margin: '0px',
        borderRadius: '0px',
        width: '272px',

        border: 'none',
        backgroundColor: '#ebecf0',

    },
    addListStyle: {
        paddingLeft: '0px',
    },

    boardCanvas: {
        borderBottomRightRadius: '3px',
        borderBottomLeftRadius: '3px',

    }
});
