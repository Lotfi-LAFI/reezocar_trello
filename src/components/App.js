import React from 'react';
import logo from '../logo.svg';
import '../App.css';
import List from './lists/AddList';
import Lists from './lists/Lists';
import { StyleSheet, css } from 'aphrodite';
import {ListGroup, Accordion, Card, Form} from 'react-bootstrap';

function App() {
  return (
          <div>
              <Lists />
          </div>
  );
}

export default App;
